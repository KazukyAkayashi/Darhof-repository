Free ressources :
- gameart (pixelart)
- raw from my camera (Fujifilm XT1 and Sigma Foveon camera)
- sound effects
- mapping (counter-strike 2d)
- lightroom and darktable preset
- and other

## Links
- [Site](https://alexisanseeuw.fr)
- [Mastodon](https://mamot.fr/@KazukyAkayashi)
- [Donate](https://liberapay.com/KazukyAkayashi/donate)
